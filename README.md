# terminus-simulation

## Continuous integration

Please refer to the [Bitbucket Pipelines](https://bitbucket.org/ekumen/terminus-simulation/addon/pipelines/home#!/).

## Gazebo

This repository contains two [Gazebo](https://bitbucket.org/osrf/gazebo) 8 plugins. One is used to render maps file structures from [OSRF Manifold library](https://bitbucket.org/osrf/manifold) into Gazebo space and the other is used to drive a car through the city. To compile and run both plugins, please visit this [link](https://bitbucket.org/ekumen/terminus-simulation/src/master/gazebo/)


## Docker

In docker directory you will find a set if folders with different docker images that run Gazebo and all its dependencies for the plugins. It is used for developing and running purposes. Please refer to this [link](https://bitbucket.org/ekumen/terminus-simulation/src/master/docker/) for further information. 


## Drake

Also, this repository contains some programs related to [Drake Simulator](http://drake.mit.edu/). Please, refer to this [link](https://bitbucket.org/ekumen/terminus/terminus-simulation/src/master/drake/) for further information.

## General repository configurations

Please add the pre-commit hook before commiting:

```
ln tools/pre-commit.sh .git/hooks/pre-commit
```
