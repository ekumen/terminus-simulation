# Drake Docker images

## Description

In this directory you will find different Drake relative dockerfiles.

## Available images

You can find the following images:

* ```drake```: it contains all the dependencies to Drake, Ignition Math and Manifold. Also you have system wide installed Ignition Math and Manifold. [Drake's repository](https://github.com/ekumenlabs/drake) is a clone of the Ekumenlabs fork.

## Graphic card drivers

At the moment there are only one graphic card driver and it's for Intel. We will support NVidia in the future.

## Default user

Use ```drake``` user as default. It's a passwordless sudo too.

## Building the image

To build the image:

```
make drake
```

## Running a container

To run a container:

```
./run.sh drake drake
```

## Drake's fork braches

You can checkout to the following branches:

* docker: it has under `[DRAKE_REPOSITORY]/drake/tools/docker` a Dockerfile with experimental things.
* sdf_sample: it has a program under `[DRAKE_REPOSITORY]/drake/sdf_sample` that can load a SDF file into drake. For more information see this [link](https://github.com/ekumenlabs/drake/tree/sdf_sample/drake/sdf_sample)
* manifold_samples: it has a program under [DRAKE_REPOSITORY]/drake/examples/manifold_samples that links to Manifold and Ignition Math 2. For more information see this [link](https://github.com/ekumenlabs/drake/tree/manifold_integration/drake/examples/manifold_samples)
* mcity_test: it uses sdf_sample program to load MCity obj file from a SDF. For more information see this [link](https://github.com/ekumenlabs/drake/tree/mcity_test/drake/sdf_sample)
