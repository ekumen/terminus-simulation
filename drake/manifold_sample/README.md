# manifold_sample

## Description

This sample project is intended to show how to configure, compile and link a project inside Drake's Bazel structure with Ignition Math and Manifold installed on the system.

It uses a Dockerfile, that prepares the environment with all the necessary tools for running the project.

## Compilation

To compile this project (it is suposed to be under <path_to_drake_repo>/drake/examples/):

```
bazel build drake/example/manifold_sample:manifold_sample
```

## Running

To run this project:

```
bazel run drake/example/manifold_sample:manifold_sample
```

You should see as the console output all the waypoints in [manifold/test/rndf/sample1.rndf](https://bitbucket.org/osrf/manifold/src/f82a77f5d7de89af9c227eea42c91581d22a5cfc/test/rndf/sample1.rndf?at=default&fileviewer=file-view-default).

