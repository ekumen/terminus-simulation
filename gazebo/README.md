# Gazebo plugins

## Dependencies

The following dependencies are required to compile manifold from source:

 - cmake
 - git
 - cppcheck
 - C++ compiler with c++11 support (eg. GCC>=4.8).
 - Gazebo
 - Manifold
 - Ignition math and messages
 - Google's Protobuf
 - Docker images in this repository

## Installation

Standard installation can be performed in UNIX systems using the following steps (or in the Docker container):

```
cd ~/ws/terminus-simulation
mkdir build
cd build
cmake -DCMAKE_PREFIX_PATH=/tmp/ign-rndf/build/ ..
make -j4
```

## Run on the docker container or in your UNIX system:

```
cd ~/ws/terminus-simulation
source setup.bash
gazebo --verbose <world_file>
```

Use:

 * --verbose: prints internal Gazebo messages and the plugin messages too.

You can use any of the samples located in /home/gazebo/ws/samples/gazebo_8 for this purpose.

## RNDF Plugin

### Description

This plugin is in charge of creating from Manifold parsing all the waypoints, roads, perimeters, etc that represent the map into Gazebo space.

### World file plugin description

Your world file must have the following node inside the world node:

```
<plugin name='rndf_gazebo_plugin0' filename='librndf_gazebo_plugin0.so.0.0.1'>
  <rndf>darpa.rndf</rndf>
  <lanes>true</lanes>
  <waypoints>true</waypoints>
  <junctions>true</junctions>
  <perimeter>true</perimeter>
  <waypoints_material>Gazebo/White</waypoints_material>
  <lane_material>Gazebo/Black</lane_material>
  <junction_material>Gazebo/Residential</junction_material>
  <perimeter_material>Gazebo/Blue</perimeter_material>
  <interpolation_distance>10.0</interpolation_distance>
  <origin>38.875413 -77.205045 12.3</origin>
  <print_labels>true</print_labels>
</plugin>
```

* The tag `rndf` is mandatory. It includes the file path of the RNDF file.
* The `lanes` and `waypoint` tags are optional and its content should be true or false indicating if you want to print the lanes and or the waypoints.
* `junctions` also is optional and enable rendering the junctions.
* `perimeter` also is optional and enable rendering the perimeters.
* `waypoints_material` change the material applied to the waypoints.
* `print_labels` is optional, and lets you enable labels over the lanes
and the waypoints to display their name.
* `lane_material` change the material applied to the lanes.
* `junction_material` change the material applied to the junctions.
* `perimeter_material` change the material applied to the perimeters.
* `origin` is option and is made by a tridimensional double vector. The first value is the latitude (*38.875413*), the second one (*-77.205045*) is latitude and the last one is elevation (*12.3*). Both latitude and longitude must be expressed in degrees, and elevation in meters.

## Driver Plugin

### Description

This plugin is in charge of moving a vehicle through the city. It communicates with rndf_gazebo_plugin to retrieve the possible entries and exits and select which way to go.

### World file plugin description

Your world file must have the following node inside the vehicle node:

```
<plugin name="driver_plugin" filename="libroad_driver_plugin.so" >
  <linearVelocity>22.5</linearVelocity>
  <initialWaitTime>1</initialWaitTime>
  <initialSegment>10</initialSegment>
  <initialLane>2</initialLane>
  <initialWaypoint>1</initialWaypoint>
</plugin>
```

Reference:

* `linearVelocity` is the maximum value of the linear velocity.
* `angularVelocity` is the maximum value of the angular velocity.
* `updatePeriod` is the value of the period in which the controller will work.
* `minimalDistance` is the value of the distance at which the vehicle considers it has reached a waypoint.
* `initialWaitTime` is the time in seconds it waits for the rndf_gazebo_plugin initialization.
* `initialSegment` is the initial segment value.
* `initialLane` is the initial lane value.
* `initialWaypoint` is the initial waypoint value.
* `axisXP` is the proportional value of the PID controller.
* `axisYP` is the proportional value of the PID controller.
* `axisZP` is the proportional value of the PID controller.
* `axisRollP` is the proportional value of the PID controller.
* `axisPitchP` is the proportional value of the PID controller.
* `axisYawZP` is the proportional value of the PID controller.

## Tests

This project includes GTests. To compile them, on the build directory after running cmake:

    ```
    make -j4
    make test
    ```

And you should see the test results displayed on the console.

## Creating a new plugin

These are required steps to create a new plugin and insert it into the compilation process.

* Create a folder called: "your_plugin_name_plugin". Add the folders "src" and "include" inside it.
* Drop all your source files with ".cc" suffix into "your_plugin_name_plugin/src" folder.
* Drop all your header files with ".hh" suffix into "your_plugin_name_plugin/include" folder.
* Add into terminus-simulation/gazebo/CMakelist.txt file the directory into the `if (BUILD_IGNITION)` section like: `add_subdirectory(your_plugin_name_plugin/)`
* Add to the CHECK_DIRS variable in terminus-simulation/tools/code_check.sh file the new path to check (include and src directories).