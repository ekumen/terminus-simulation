# Gazebo compilation image

In order to guarantee that we are all using the same gazebo configuration we have created a docker image that downloads and compiles Gazebo 8.

For the rest of the guide we will use {REPOSITORY_PATH} as the base path of both terminus-simulation and [terminus](https://github.com/ekumenlabs/terminus) repositories.

## Docker image build

The available images names are the following:

| Image Target             | Description |
|------------------------------|-------------|
| ekudev                       | This a base image for development purposes |
| gazebo8                      | This image is based on ekudev. It adds Gazebo 8 from debs. |
| gazebo8-terminus             | This image is based on gazebo8. It adds ignition-rndf from source. |
| gazebo8-terminus-intel       | This image is based on gazebo8-terminus and adds Intel Graphics driver. |
| gazebo8-terminus-nvidia      | This image is based on gazebo8-terminus and adds NVidia Graphics driver. |


To build them, just call make:

```
cd {REPOSITORY_PATH}/terminus-simulation/gazebo/docker
$ make [IMAGE_TARGET]
```

## Usage


| Name                         | Tag    | Function                                                                                                                                  |
|------------------------------|--------|---------------------------------|
| ekudev                       | latest | This a base image for development purposes |
| gazebo8                      | latest | This image is based on ekudev. It adds Gazebo 8 from debs. |
| gazebo8-terminus             | latest | This image is based on gazebo8. It adds ignition-rndf from source. |
| gazebo8-terminus-intel       | latest | This image is based on gazebo8-terminus and adds Intel Graphics driver. |
| gazebo8-terminus-nvidia      | latest | This image is based on gazebo8-terminus and adds NVidia Graphics driver. |

To start the container just execute the script `run_simulator`:

```
cd {REPOSITORY_PATH}/terminus-simulation/gazebo/docker
$ ./run_simulator [IMAGE_NAME] [CONTAINER_NAME:TAG]
```

We recommend to use the same name for the container, so you should run one of these commands for example:

_Running a NVidia container:_

```
$ ./run_simulator gazebo8-terminus-nvidia gazebo8-terminus-nvidia
```

_Running a Intel container:_

```
$ ./run_simulator gazebo8-terminus-intel gazebo8-terminus-intel
```

The script `run_simulator` is configured to mount 2 directories:

* Current repository folder (from docker folder, its parent directory)
* `samples` directory from [terminus](https://github.com/ekumenlabs/terminus) repository.

In case the `terminus-simulation` repository is not at the same level of the current repository, you should change the mount path in the `run_simulator` script (check `DOCKER_MOUNT_ARGS` variable).

We recommend working outside the container and placing the files in folder of the repositories. BEWARE THAT THE CONTAINER WILL BE DELETED EACH TIME THAT YOU EXIT IT. This is done in order to make sure that you are not modifying the image that you are working with, so all the collaborators are working with the same image.

## Dependencies

### Current repositories commits

In the following table you can check the current repositories commits used for tag.

| Project       | Description                              |
|---------------|------------------------------------------|
| Gazebo        | Used from debs, now we are downloading the last revision from 8 version. |
| Ignition-RNDF | [214a333fbdcbabf918d779533b2a63de2f9bb57a](https://bitbucket.org/ignitionrobotics/ign-rndf/src/214a333fbdcbabf918d779533b2a63de2f9bb57a/?at=ignition-rndf_0.1.5) |


### Checking Ignition-RNDF version

This docker compiles [ignition-rndf](https://bitbucket.org/ignitionrobotics/ign-rndf) on a previously declared commit. Please make sure that you are compiling the right commit and branch. You can always check what commit it is using by doing the following inside the image:

```
$ cd /tmp/ign-rndf
$ hg --debug id -i
```

## Special considerations regarding the graphic card usage

It is important to have the exact version of the graphics card in your computer and in the docker container. As a result we have different Dockerfiles for different drivers.

### Using NVIDIA drivers

We are using the proprietary drivers from `ppa:graphics-drivers/ppa`. The default driver is `nvidia-367`. From the docker folder execute:

```
cd {REPOSITORY_PATH}/terminus-simulation/gazebo/docker
make gazebo8-terminus-nvidia gazebo8-terminus-nvidia
```

If you are using another one you can use:

`make NVIDIA_DRIVER="nvidia-xxx" gazebo-terminus-nvidia gazebo-terminus-nvidia`

Check that your NVidia driver that is going to be downloaded is the same that the one that you have installed.

### Using Intel drivers

For Intel, you need to run the following command:
```
cd {REPOSITORY_PATH}/terminus-simulation/gazebo/docker
make gazebo8-terminus-intel gazebo8-terminus-intel
```

Make sure that your host computer has the latest version of the package `xserver-xorg-video-intel` that will be downloaded.
