/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#ifndef DYNAMIC_RENDER_HH
#define DYNAMIC_RENDER_HH

#include <gazebo/gazebo.hh>
#include <gazebo/util/system.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/rendering/rendering.hh>
#include <gazebo/rendering/RenderTypes.hh>
#include <gazebo/transport/TransportTypes.hh>
#include <gazebo/transport/Publisher.hh>
#include <gazebo/gui/GuiIface.hh>
#include <gazebo/gui/EntityMaker.hh>
#include <gazebo/gui/GuiEvents.hh>
#include <gazebo/math/Vector3.hh>
#include <gazebo/common/common.hh>
#include <gazebo/common/Console.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/rendering/UserCamera.hh>
#include <gazebo/math/Quaternion.hh>
#include <gazebo/common/MouseEvent.hh>
#include <gazebo/common/Exception.hh>

#include <gazebo/common/Mesh.hh>
#include <gazebo/common/MeshManager.hh>

#include <sdf/Element.hh>
#include <ignition/math.hh>
#include <ignition/msgs.hh>
#include <ignition/transport.hh>

#include <ignition/rndf/RNDF.hh>
#include <ignition/rndf/Segment.hh>
#include <ignition/rndf/Lane.hh>
#include <ignition/rndf/Waypoint.hh>
#include <ignition/rndf/Exit.hh>
#include <ignition/rndf/UniqueId.hh>
#include <ignition/rndf/Zone.hh>
#include <ignition/rndf/Perimeter.hh>
#include <ignition/rndf/ParkingSpot.hh>
#include <ignition/rndf/RNDFNode.hh>

#include <fstream>
#include <streambuf>
#include <sstream>
#include <chrono>
#include <thread>
#include <string>
#include <vector>
#include <algorithm>

#include "rndf_plugin_helpers.hh"
#include "road_creator.hh"
#include "waypoint_creator.hh"
#include "camera_controller.hh"
#include "projection.hh"
#include "perimeter_creator.hh"
#include "junction_creator.hh"

#include "mesh_creator.hh"

#include "./unique_id.pb.h"
#include "./waypoints_request.pb.h"
#include "./waypoints_response.pb.h"

namespace gazebo {

class RNDF_PLUGIN_VISIBLE DynamicRender{
  typedef const boost::shared_ptr<
    const rndf_gazebo_plugin_msgs::msgs::WaypointsRequest> WaypointsRequestPtr;

  private : void LoadRoadMesh();
  /// \brief Default constructor.
  public: DynamicRender();
  /// \brief Destructor.
  public: virtual ~DynamicRender();
  /// \brief It loads the Update event handler and tries to load the
  ///  RNDF file.
  public: void Load(gazebo::physics::WorldPtr _parent, sdf::ElementPtr _sdf);
  /// \brief This method is the callback handler of the topic that listens
  /// to incomming messages from the road driver pluglin.
  /// \details It receives a waypoint Id and then it loads different vectors,
  /// such as the control points positions, the road waypoint ids, the entry
  /// and exits.
  /// \param[in] wpId a pointer to a message that describes the uniqueId of
  /// the vertex in the graph
  public: void OnPublishWaypoints(const ignition::rndf::UniqueId &wpId);
  /// \brief Answer Waypoint Request Messages publishing a Waypoint Response
  // message with the lane waypoints positions, entries ids and entries
  // positions
  // \param _msg message received
  public: void OnWaypointsRequestsMsgs(WaypointsRequestPtr &_msg);

  /// \brief It loads a sample RNDF file using Manifold Library
  /// \throws gazebo::common::Exception If the RNDF file isn't successfully
  /// loaded
  private: void LoadRNDFFile();
  /// \brief It prints the RNDF file stats using Manifold Library
  private: void PrintRNDFStats() const;
  /// \brief It loads the segments in Gazebo
  /// \param[in] segments It is the vector of segments to draw
  private: void LoadSegments(
    const std::vector<ignition::rndf::Segment> &segments);
  /// \brief It loads the lanes in Gazebo
  /// \param[in] segmentParent It is the parent segment id
  /// \param[in] lanes It is the vector of lanes to draw
  private: void LoadLanes(const int segmentParent,
    const std::vector<ignition::rndf::Lane> &lanes);
  /// \brief It loads junctions between segments and perimeters
  /// in Gazebo
  private: void LoadJunctions();
  /// \brief It loads waypoints setting a custom type to all
  /// of them
  /// \param[in] segmentParent It is the parent segment id
  /// \param[in] laneParent It is the parent lane
  /// \param[in] waypoints A STL vector containing all the waypoints
  /// \param[in] type The type of waypoint to represent
  private: void LoadWaypoints(const int segmentParent,
    const int laneParent,
    const std::vector<ignition::rndf::Waypoint> &waypoints,
    const gazebo::WayPointCreator::Type type
      = gazebo::WayPointCreator::Type::DEFAULT);
  /// \brief It finds a waypoint by its unique id
  /// \param[in] waypointId It is an object that represents the id of the
  /// waypoint inside the RNDF file.
  /// \return The pointer to the waypoint if that matches the waypoint id
  private: ignition::rndf::Waypoint* GetWaypointByUniqueId(
    const ignition::rndf::UniqueId &waypointId) const;
  /// \brief It gets the lane reference by a valid waypoint into it
  /// \param[in] waypointId It is the reference to the waypoint
  /// \return A reference to the lane if it was found
  /// \throws gazebo::common::Exception if the lane is not found.
  private: ignition::rndf::Lane* GetLaneByUniqueId(
    const ignition::rndf::UniqueId &waypointId) const;
  /// \brief It creates a lane name like 'lane_SId_LId'
  /// \param[in] segmentParentId It is the segment parent Id
  /// \param[in] laneId It is the lane Id
  /// \return A std::string in the form 'lane_SId_LId'
  private: std::string CreateLaneName(const int segmentParentId,
    const int laneId) const;
  /// \brief It finds all the lane waypoints, and loads its positions.
  /// \param[in] laneName It is the name of the lane
  /// \param[in] lane A reference to the lane, to get the size
  /// of the waypoints.
  /// \param[out] waypointPositions A vector to fill with the
  /// waypoint positions.
  private: void FillLaneWaypointPositions(const std::string &laneName,
    const ignition::rndf::Lane &lane,
    std::vector<ignition::math::Vector3d> &waypointPositions) const;
  /// \brief It fills a vector with all the control points of a road given an
  /// interpolation distance.
  /// \param[in] laneName It is the name of the lane
  /// \param[in] lane A reference to the lane, to get the size of
  /// the waypoints.
  /// \param[out] lanePoints A vector to fill with the interpolated
  /// waypoint positions.
  private: void FillLanePoints(const std::string &laneName,
    const ignition::rndf::Lane &lane,
    std::vector<ignition::math::Vector3d> &lanePoints);
  /// \brief It parses the SDF file to get the plugin arguments
  /// \param[in] sdfPtr This is the file path of the sdf world
  /// \throws gazebo::Exception in case there is a problem
  /// finding the plugin node.
  private: void ParseSDF(const sdf::ElementPtr sdfPtr);
  /// \brief It loads all the zones attributes.
  /// \param[in] zones A reference to a vector containing the zones
  private: void LoadZones(const std::vector<ignition::rndf::Zone> &zones);
  /// \brief It loads the waypoints, the polygon of the perimeter
  /// and the junctions to it.
  /// \param[in] zoneId It is the id of its parent zone.
  /// \param[in] perimeter A reference to the perimeter.
  private: void LoadPerimeter(const int zoneId,
    const ignition::rndf::Perimeter &perimeter);
  /// \brief It creates a perimeter polygon
  /// \param[in] waypoints A reference to the waypoints that define the
  /// verteces of the waypoints.
  private: void CreatePerimeter(
    const std::vector<ignition::rndf::Waypoint> &waypoints);
  /// \brief It loads the waypoints of the parking spots
  /// \param[in] zoneId The id of the spot's zone.
  /// \param[in] spots A reference to a vector containing the zones.
  private: void LoadParkingSpots(const int zoneId,
    const std::vector<ignition::rndf::ParkingSpot> &spots);
  /// \brief It creates the junctions and draws them for the segments
  /// \param[in] junctions A vector, empty or not, with the junctions.
  /// \param[in] segments A vector of segments where to find exit entry
  /// couples with junctions.
  private: void GenerateJunctions(
    std::vector<gazebo::Junction> &junctions,
    const std::vector<ignition::rndf::Segment> &segments) const;
  /// \brief It creates the junctions and draws them for the segments
  /// \param[in] junctions A vector, empty or not, with the junctions.
  /// \param[in] segments A vector of segments where to find exit entry
  /// couples with junctions.
  private: void GenerateJunctions(
    std::vector<gazebo::Junction> &junctions,
    const std::vector<ignition::rndf::Zone> &zones) const;

  /// \brief It gets all the exits from a segment
  /// \param[in] segment A reference to the segment
  /// \return A vector with all the exits of the segment
  private: std::vector<ignition::rndf::Exit> GetSegmentExits(
    const ignition::rndf::Segment &segment) const;
  /// \brief It fills the junction vector or modifies an element of it
  /// given a new vector of exits.
  /// \brief exits A reference to the exits vector
  /// \brief A reference to the junctions vector
  private: void FillJunctions(
    const std::vector<ignition::rndf::Exit> &exits,
    std::vector<gazebo::Junction> &junctions) const;
  /// \brief It creates a lane termination object from the unique id
  /// \param[in] id The id of the waypoint to look for.
  /// \return A lane termination object
  private: gazebo::LaneTermination CreateLaneTermination(
    const ignition::rndf::UniqueId &id) const;
  /// \brief It gets a vector containing the lat, long and elevation
  /// from the location of a waypoints.
  /// \param[in] wp A waypoint to get its location
  /// \return A vector containing lat, long and elevation coordinates.
  private: ignition::math::Vector3d GetWaypointSphericalLocation(
    const ignition::rndf::Waypoint &wp) const;
  /// \brief It gets all the spherical locations of all the waypoints
  /// \param[out] A vector to load all the positions.
  private: void GetAllWaypointLocations(
    std::vector<ignition::math::Vector3d> &positions) const;

  /// \brief It retrieves the poses of a list of waypoints supposed
  /// that all waypoinst are in the same lane.
  /// \param[in] waypoints It is vector of waypoints to retrieve the
  /// the poses
  /// \return A vector of poses of the waypoints
  private: std::vector<ignition::math::Pose3d> GetWaypointsPoses(
    const std::vector<ignition::rndf::Waypoint> &waypoints) const;
  /// \brief It gets the pose of a waypoint given a waypoint and the
  /// next waypoint.
  /// \param[in] waypoint It is the waypoint to retrive its pose
  /// \param[in] nextWaypoint It is the following waypoint in the lane
  /// \return The pose of the waypoint
  private: ignition::math::Pose3d GetWaypointPose(
    const ignition::rndf::Waypoint &waypoint,
    const ignition::rndf::Waypoint &nextWaypoint) const;
  /// \brief It gets teh waypoint pose
  /// \param[in] segmentId The segment id.
  /// \param[in] laneId The lane id
  /// \param[in] waypoint The reference to the waypoint
  /// \return The pose of the waypoint
  private: ignition::math::Pose3d GetWaypointPose(
    const uint segmentId,
    const uint laneId,
    const ignition::rndf::Waypoint &waypoint) const;
  /// \brief This is the pose of the last waypoint
  /// \param[in] prevQuaternion It is the value of the
  /// quaternion in the waypoint pose.
  /// \param[in] waypoint A reference to the waypoint
  /// \return A pose object that is constructed from the same
  /// quaternion from the previous one and the position of the
  /// last.
  private: ignition::math::Pose3d GetLastWaypointPose(
    const ignition::math::Quaterniond &prevQuaternion,
    const ignition::rndf::Waypoint &waypoint) const;
  /// \brief It tells if the waypoint is the last of the lane
  /// \param[in] waypoint A reference to the waypoint
  /// \param[in] lane A reference to the lane which the waypoint
  /// belongs to
  /// \return True if the waypoint is the last of the lane.
  private: bool IsLastWaypoint(
    const ignition::rndf::Waypoint &waypoint,
    const ignition::rndf::Lane &lane) const;
  /// \brief It finds the previous waypoint
  /// \param[in] segmentId The id of the segment of the waypoint
  /// \param[in] laneId The id of the lane
  /// \param[in] waypoint The reference to the waypoint
  /// \return A waypoint that is before the current one.
  private: ignition::rndf::Waypoint GetPreviousWaypoint(
    const uint segmentId,
    const uint laneId,
    const ignition::rndf::Waypoint &waypoint) const;
  /// \brief It finds the next waypoint
  /// \param[in] segmentId The id of the segment
  /// \param[in] laneId The id of the lane
  /// \param[in] waypoint The id of the waypoint
  /// \return A waypoint that is the following in the lane list.
  private: ignition::rndf::Waypoint GetNextWaypoint(
    const uint segmentId,
    const uint laneId,
    const ignition::rndf::Waypoint &waypoint) const;
  /// \brief It creates a waypoint name like 'waypoint_SId_LId_WId'
  /// \param[in] segmentParentId It is the segment parent id
  /// \param[in] laneParentId It is the lane parent id
  /// \return A std::string in the form 'waypoint_SId_LId_WId'
  private: std::string CreateWaypointName(const int segmentParentId,
    const int laneParentId,
    const int waypointId) const;
  /// \brief It computes the lane control points with
  /// interpolation if needed.
  /// \param[in] lane It is the lane reference
  /// \return A vector with the position of the control points.
  private: std::vector<ignition::math::Vector3d> GetLanePoints(
    const ignition::rndf::Lane &lane) const;
  /// \brief It keeps track of the times the Update function is called
  private: int count;
  /// \brief It holds the visual message that is created to create waypoints
  private: msgs::Visual *visualMsg;
  /// \brief It keeps the connections of the handlers like Update function
  private: std::vector<event::ConnectionPtr> connections;
  /// \brief Utiliy object that creates roads
  private: gazebo::RoadCreator roadCreator;
  /// \brief Utility object that creates waypoints
  private: gazebo::WayPointCreator waypointCreator;
  /// \brief Manifold object tha loads into memory the RNDF file
  private: std::shared_ptr<ignition::rndf::RNDF> rndfInfo;
  /// \brief It is the parsed argument to know if waypoints are required
  private: bool printWaypoints;
  /// \brief It is the parsed argument to know if waypoints are required
  private: bool printLanes;
  /// \brief It is the parsed argument to know if perimeters are required
  private: bool printPerimeter;
  /// \brief It is the parsed argument to know if junctions are required.
  private: bool printJunctions;
  /// \brief It is the parsed RNDF file path argument
  private: std::string filePath;
  /// \brief It has the waypoint material string
  private: std::string waypointMaterial;
  /// \brief It has the lane material string
  private: std::string laneMaterial;
  /// \brief It has the junction material string
  private: std::string junctionMaterial;
  /// \brief It has the perimeter material string
  private: std::string perimeterMaterial;
  /// \brief This is the interpolation distance parameter for the roads.
  private: double interpolationDistance;
  /// \brief This is the origin to refer the RNDF projection from Global
  /// to Local frames.
  private: ignition::math::SphericalCoordinates origin;
  /// \brief Used to set the origin in case it appears in the RNDF file.
  private: bool originIsSet;
  /// \brief Defines labels should be displayed or not
  private: bool printLabels;
  /// \brief Wrapper to control the camera
  private: gazebo::CameraController cameraController;
  /// \brief It has the projection methods to convert coordinates
  private: gazebo::Projection projection;
  /// \brief It keeps track of the sdf plugin node
  private: sdf::ElementPtr sdfPtr;
  /// \brief It is the creator of the perimeter
  private: gazebo::PerimeterCreator perimeterCreator;
  /// \brief A junction creator to create the junctions
  private: gazebo::JunctionCreator junctionCreator;
  /// \brief A reference to the parent world pointer.
  private: gazebo::physics::WorldPtr worldPtr;
  /// \brief Subscriber to get path messages
  private: transport::SubscriberPtr waypointsRequestsSubscriber;
  /// \brief Transport node used to communicate with the transport system
  private: transport::NodePtr node;

  private: gazebo::MeshCreator meshCreator;
};

}

#endif
