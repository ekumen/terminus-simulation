/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#ifndef JUNCTION_CREATOR_HH
#define JUNCTION_CREATOR_HH

#include <gazebo/gazebo.hh>
#include <gazebo/util/system.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/rendering/rendering.hh>
#include <gazebo/rendering/RenderTypes.hh>
#include <gazebo/transport/TransportTypes.hh>
#include <gazebo/transport/Publisher.hh>

#include <ignition/math.hh>
#include <ignition/msgs.hh>
#include <ignition/transport.hh>

#include <ignition/rndf/RNDF.hh>
#include <ignition/rndf/Segment.hh>
#include <ignition/rndf/Lane.hh>
#include <ignition/rndf/Waypoint.hh>
#include <ignition/rndf/Exit.hh>
#include <ignition/rndf/UniqueId.hh>

#include <iostream>
#include <string>
#include <vector>
#include <functional>

#include "rndf_plugin_helpers.hh"
#include "math_util.hh"
#include "creator.hh"
#include "mesh_creator.hh"

namespace gazebo {

/// \brief It manages the waypoint, its pose and the extent points
/// of the lane
class RNDF_PLUGIN_VISIBLE LaneTermination {
  /// \brief Default constructor
  public: LaneTermination();
  /// \brief Constructor that handles all the instance variables
  /// \param[in] id The unique id of the waypoint.
  /// \param[in] pose The pose of the waypoint
  /// \param[in] laneWidth The width of the lane
  public: LaneTermination(const ignition::rndf::UniqueId &id,
    const ignition::math::Pose3d &pose,
    const double laneWidth);
  /// \brief Destructor
  public: ~LaneTermination();
  /// \brief It gives a reference to the id
  /// \return A reference to the uniqueId
  public: ignition::rndf::UniqueId& Id();
  /// \brief It gives a reference to the id
  /// \return A reference to the uniqueId
  public: const ignition::rndf::UniqueId& Id() const;
  /// \brief It gives a reference to the pose
  /// \return A reference to the pose
  public: ignition::math::Pose3d& Pose();
  /// \brief It gives a reference to the width of the lane
  /// \return A reference to the width
  public: double& LaneWidth();
  /// \brief A vector that has the extent points of the lane
  /// \return A vector with the extent points positions
  public: std::vector<ignition::math::Vector3d> ExtentPositions() const;
  /// \brief It manages the comparison by id of the
  /// lane termination
  struct IdMatch :
    public std::unary_function<ignition::rndf::UniqueId, bool>{
    /// \brief Constructor
    /// \param[in] id The id to compare to
    explicit IdMatch(const ignition::rndf::UniqueId &_id) : id(_id) {}
    /// \brief The id to make the comparisson
    ignition::rndf::UniqueId id;
    /// \brief The function operator to call to validate
    /// the comparison
    /// \param[in] _id The value of the UniqueId to compare
    /// against id
    /// \return A boolean indcating true if it matches or false
    /// if not
    bool operator()(const ignition::rndf::UniqueId &_id) {
      if (id == _id)
        return true;
      return false;
    }
  };

  /// \brief The id of the waypoint
  private: ignition::rndf::UniqueId id;
  /// \brief The pose of the waypoint
  private: ignition::math::Pose3d pose;
  /// \brief The width of the lane where the waypoint is
  private: double laneWidth;
};

/// \brief This class handles all the junction lane terminations
/// and computes the points in such order that the visualization
/// is nice.
class RNDF_PLUGIN_VISIBLE Junction {
  /// \brief Default constructor
  public: Junction();
  /// \brief Constructor
  /// \param[in] laneTermination It gives a reference to the
  /// laneTerminations vector
  public: Junction(
    const std::vector<gazebo::LaneTermination>& laneTerminations);
  /// \brief Destructor
  public: ~Junction();
  /// \brief Accessor to the LaneTermination vector
  /// \return A reference to the LaneTermination vector
  public: std::vector<gazebo::LaneTermination>& LaneTerminations();
  /// \brief It computes all the items of teh LaneTermination
  /// points and then it orders them in a polar way so the
  /// visualizaton works.
  public: void Compute();
  /// \brief Accessor of the pose of the junction. It is the
  /// center.
  /// \return A reference to the pose
  public: ignition::math::Pose3d& Pose();
  /// \brief Accessor for the points of the junctions
  /// \return A reference to a vector which contains the value
  /// of the points
  public: std::vector<ignition::math::Vector3d>& Points();
  /// \brief It checks if any of the lane terminations contains
  /// at least one of UniqueIds passed
  /// \param[in] ids A vector of UniqueIds to match
  /// \return True if any of the ids is matched or false if not
  public: bool ContainsAny(
    const std::vector<ignition::rndf::UniqueId> &ids) const;
  /// \brief It finds the junction that matches by ids
  /// \param[in] junctions A vector refence of junctions
  /// \param[in] lts A vector reference of lane terminations
  /// \return The id of the junction inside the vector or -1 if
  /// it is not found
  public: static int GetJunctionIndexById(
    const std::vector<gazebo::Junction> &junctions,
    const std::vector<gazebo::LaneTermination> &lts);

  /// \brief It prints the points positions of the junction
  private: void PrintPoints() const;
  /// \brief A vector of laneTerminations
  private: std::vector<gazebo::LaneTermination> laneTerminations;
  /// \brief A vector of points that contains all the extents
  /// of the lane terminations
  private: std::vector<ignition::math::Vector3d> points;
  /// \brief The pose of the center of the junction.
  private: ignition::math::Pose3d pose;
};

/// \brief This is an utility class that handles the marker
/// message creation to create a junction.
class RNDF_PLUGIN_VISIBLE JunctionCreator : public Creator {
  /// \brief Constructor
  public: JunctionCreator();
  /// \brief Destructor
  public: ~JunctionCreator();
  /// \brief Setter for the junction reference
  /// \param[in] junction The junction to draw.
  public: void Junction(const gazebo::Junction &junction);
  /// \brief Setter for the material reference
  /// \param[in] material A string representation of the material
  public: void Material(const std::string &material);
  /// \brief It creates a convex polygon given the a set of
  /// verteces. It works with concave polygons too, but the
  /// baricenter of the polygon must be inside the perimeter.
  /// \return True if the message is sent, and false if it fails.
  public: bool Create();

  public: void SetMeshCreator(gazebo::MeshCreator *_meshCreator);

  /// \brief The reference to the junction
  private: gazebo::Junction junction;
  /// \brief The reference to the material
  private: std::string material;

  private: gazebo::MeshCreator *meshCreator;
};

}

#endif
