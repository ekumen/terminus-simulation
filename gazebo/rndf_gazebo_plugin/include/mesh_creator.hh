/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#ifndef MESH_CREATOR_HH
#define MESH_CREATOR_HH

#include <gazebo/gazebo.hh>
#include <gazebo/common/common.hh>
#include <gazebo/common/Mesh.hh>
#include <ignition/math.hh>

#include <memory>
#include <vector>
#include <string>
#include <iostream>

#include "rndf_plugin_helpers.hh"

namespace gazebo {

class RNDF_PLUGIN_VISIBLE MeshCreator {
  public: MeshCreator();
  public: ~MeshCreator();
  public: void Name(const std::string &_name);
  public: void AddVertexes(
    const std::vector<ignition::math::Vector3d> &_vertexes);
  public: gazebo::common::Mesh* Create() const;
  public: void ClearVertexes();

  private: std::string name;
  private: std::vector<ignition::math::Vector3d> vertexes;
};

}

#endif
