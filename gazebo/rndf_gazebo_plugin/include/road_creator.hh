/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#ifndef ROAD_CREATOR_HH
#define ROAD_CREATOR_HH

#include <gazebo/gazebo.hh>
#include <gazebo/util/system.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/rendering/rendering.hh>
#include <gazebo/rendering/RenderTypes.hh>
#include <gazebo/transport/TransportTypes.hh>
#include <gazebo/transport/Publisher.hh>


#include <ignition/math.hh>
#include <ignition/msgs.hh>
#include <ignition/transport.hh>

#include <iostream>
#include <string>
#include <vector>

#include "rndf_plugin_helpers.hh"
#include "text_creator.hh"
#include "creator.hh"
#include "mesh_creator.hh"
#include "math_util.hh"

namespace gazebo {

/// \brief  Utility class to make road visuals on Gazebo
class RNDF_PLUGIN_VISIBLE RoadCreator : public Creator {
  /// \brief Default constructor.
  public: RoadCreator();
  /// \brief Destructor.
  public: ~RoadCreator();
  /// \brief It creates an interpolated road with splines.
  /// It is recursive to match the minimum distance.
  /// \param[in] points the control points of the splines
  /// \param[in] distanceThreshold it is the maximum distance between
  /// two points of the road
  /// \return A vector of points that is created with splines and matches
  /// the maximum distance between all.
  public: std::vector<ignition::math::Vector3d> InterpolateRoad(
    const std::vector<ignition::math::Vector3d> &_points,
    const double distanceThreshold) const;
  /// \brief Setter for the width
  /// \param[in] _width the road width
  public: void Width(const double _width);
  /// \brief Setter for the name
  /// \param[in] _name the road visual name
  public: void Name(const std::string &_name);
  /// \brief Setter for the points
  /// \param[in] _points vector of points that make the path of the road
  public: void Points(const std::vector<ignition::math::Vector3d> &_points);
  /// \brief Setter for the material
  /// \param[in] _material The material to add to the road.
  public: void Material(const std::string &_material);
  /// \brief Sets if labels should be displayed or not.
  public: void DisplayNames(const bool _printLabels);
  /// \brief It creates a road message
  /// \return True if it can succesfuly send the message.
  public: bool Create();
  /// \brief Given a vector of points that matches the roads, it
  /// gets a vector of poses of each.
  /// \param[in] _points It is a vector of 3D points that corresponds to
  /// a road.
  /// \param[out] poses It should be an empty vector in which this function
  /// includes the pose of the points. The direction is acquired by doing
  /// a difference vector between two consecutive points. The last point gets
  /// the same direction as the last but one.
  public: void FillPoses(
    const std::vector<ignition::math::Vector3d> &_points,
    std::vector<ignition::math::Pose3d> &poses) const;
  /// \brief Given a poses and the width of the lane, it computes
  /// the points and orders them in such a way that the can be rendererd
  /// as a triangle list.
  /// \param[in] laneWidth The width of the lane
  /// \param[in] poses The vector of poses
  /// \param[out] trianglePoints A vector in which to include the calculated
  /// points
  public: void FillTriangleStripPoints(const double laneWidth,
    const std::vector<ignition::math::Pose3d> &poses,
    std::vector<ignition::math::Vector3d> &trianglePoints);
  /// \brief It creates the triangles of a segment of road.
  /// \param[in] laneWidth It is the width of the lane.
  /// \param[in] pose It is the pose of the current control point.
  /// \param[in] nextPose It is the pose of the next control point.
  /// \param[out] trianglePoints It is a vector in which the function appends
  /// the generated triangle verteces.
  public: void FillSegmentTriangles(
    const double laneWidth,
    const ignition::math::Pose3d &pose,
    const ignition::math::Pose3d &nextPose,
    std::vector<ignition::math::Vector3d> &trianglePoints);
  /// \brief It adds some triangles to smooth the connection between two lane
  /// segments.
  /// \param[in] laneWidth It is the width of the lane.
  /// \param[in] pose It is the pose of the current control point.
  /// \param[in] nextPose It is the pose of the next control point.
  /// \param[out] trianglePoints It is a vector in which the function appends
  /// the generated triangle vertices.
  public: void FillSegmentConnectionTriangles(
    const double laneWidth,
    const ignition::math::Pose3d &pose,
    const ignition::math::Pose3d &nextPose,
    std::vector<ignition::math::Vector3d> &trianglePoints);

  public: void SetMeshCreator(gazebo::MeshCreator *_meshCreator);

  /// \brief It calls CreateName every Creator::NAME_INTERPOLATION_STEPS
  /// and in case the number of control points from the last one is
  /// at least half Creator::NAME_INTERPOLATION_STEPS.
  /// \param[in] poses This are the values of the poses of each control point
  private: void CreateNames(const std::vector<ignition::math::Pose3d> &poses);
  /// \brief It creates a label for the lane over it.
  /// \brief pose It is the pose of the control point of the road.
  private: void CreateName(const ignition::math::Pose3d &pose);
  /// \brief It is the width of the road
  private: double width;
  /// \brief These are the points of the road
  private: std::vector<ignition::math::Vector3d> points;
  /// \brief It is the name of the lane
  private: std::string name;
  /// \brief It is the material of the road
  private: std::string material;
  /// \brief Text creator object for waypoint labels
  private: gazebo::TextCreator textCreator;
  /// \brief Defines labels should be displayed or not
  private: bool printLabels;

  private: gazebo::MeshCreator* meshCreator;
};

}
#endif
