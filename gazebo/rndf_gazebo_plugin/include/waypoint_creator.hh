/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#ifndef WAYPOINT_CREATOR_HH
#define WAYPOINT_CREATOR_HH

#include <gazebo/gazebo.hh>
#include <gazebo/common/common.hh>
#include <gazebo/common/Console.hh>
#include <gazebo/util/system.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/rendering/rendering.hh>
#include <gazebo/rendering/RenderTypes.hh>
#include <gazebo/transport/TransportTypes.hh>
#include <gazebo/transport/Publisher.hh>

#include <ignition/math.hh>
#include <ignition/msgs.hh>
#include <ignition/transport.hh>

#include <iostream>
#include <string>
#include <vector>
#include <functional>

#include "rndf_plugin_helpers.hh"
#include "math_util.hh"
#include "text_creator.hh"
#include "creator.hh"

namespace gazebo {

/// \brief This is an utility class to make waypoint visuals
class RNDF_PLUGIN_VISIBLE WayPointCreator : public Creator {
  /// \brief It has the different possibilities of waypoints shapes
  public: enum Shape {
    CIRCLE,
    ARROW
  };
  /// \brief It has the different types of waypoints
  public: enum Type {
    DEFAULT,
    CHECKPOINT,
    SPOT,
    ENTRY,
    EXIT,
    STOP,
    PERIMETER
  };

  /// \brief Default constructor.
  public: WayPointCreator();
  /// \brief Destructor.
  public: ~WayPointCreator();
  /// \brief Setter for the name
  /// \param[in] name The name of the road
  public: void Name(const std::string &name);
  /// \brief Setter for the pose
  /// \param[in] pose The pose of the waypoint
  public: void Pose(const ignition::math::Pose3d &pose);
  /// \brief Setter for the radius
  /// \param[in] radius The radius of the waypoint
  public: void Radius(const double radius);
  /// \brief Setter for the material
  /// \param[in] material The material name of the waypoint.
  public: void Material(const std::string &material);
  /// \brief Setter for the type
  /// \param[in] type The waypoint type.
  public: void SetType(const WayPointCreator::Type type);
  /// \brief Sets if labels should be displayed or not.
  public: void DisplayNames(const bool _printLabels);
  /// \brief It creates waypoint messages to be sent to the
  /// Gazebo server.
  /// \return True if the message has been sent.
  public: bool Create();

  /// \brief It creates the circle marker message
  /// \param[out] markerMsg A reference where to store the geometry
  /// of the circel.
  /// \param[in] radius Just a scale value.
  private: void CreateCircleGeometry(ignition::msgs::Marker &markerMsg,
    const double radius);
  /// \brief It creates the arrow marker message.
  /// \param[out] markerMsg A reference where to store the geometry.
  /// \param[in] radius Just a scale value.
  private: void CreateArrowGeometry(ignition::msgs::Marker &markerMsg,
    const double radius);
  /// \brief It displays a label with the names of the waypoint.
  private: void CreateName();
  /// \brief It keeps the points of the arrows
  private: static std::vector<ignition::math::Vector3d> arrowPoints;
  /// \brief The name of the waypoint
  private: std::string name;
  /// \brief The name of the parent visual
  private: std::string parentName;
  /// \brief The pose of the waypoint
  private: ignition::math::Pose3d pose;
  /// \brief The radius of the waypoint
  private: double radius;
  /// \brief The material of the waypoint
  private: std::string material;
  /// \brief The type of the waypoint
  private: WayPointCreator::Type type;
  /// \brief Text creator object for waypoint labels
  private: gazebo::TextCreator textCreator;
  /// \brief Defines labels should be displayed or not
  private: bool printLabels;
};

}
#endif
