/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#include "junction_creator.hh"

namespace gazebo {

LaneTermination::LaneTermination() {
}

LaneTermination::LaneTermination(
  const ignition::rndf::UniqueId &_id,
  const ignition::math::Pose3d &_pose,
  const double _laneWidth) {
  this->id = _id;
  this->pose = _pose;
  if (_laneWidth <= 0.0) {
    this->laneWidth = Creator::DEFAULT_LANE_WIDTH;
  }
  else
    this->laneWidth = _laneWidth;
}

LaneTermination::~LaneTermination() {
}

ignition::rndf::UniqueId& LaneTermination::Id() {
  return id;
}

const ignition::rndf::UniqueId& LaneTermination::Id() const {
  return id;
}

ignition::math::Pose3d& LaneTermination::Pose() {
  return pose;
}

double& LaneTermination::LaneWidth() {
  return laneWidth;
}

std::vector<ignition::math::Vector3d> LaneTermination::ExtentPositions() const {
  ignition::math::Vector3d a1, a2;
  ComputeExtents(pose.Pos(),
    pose.Rot().Yaw() + M_PI/2.0,
    laneWidth,
    a1,
    a2);
  return std::vector<ignition::math::Vector3d>{a1, a2};
}

Junction::Junction() {
}

Junction::Junction(
  const std::vector<gazebo::LaneTermination>& _laneTerminations) {
  this->laneTerminations = _laneTerminations;
}

Junction::~Junction() {
}

std::vector<gazebo::LaneTermination>& Junction::LaneTerminations() {
  return laneTerminations;
}

void Junction::Compute() {
  for (const auto &laneTermination : laneTerminations) {
    std::vector<ignition::math::Vector3d> const &extents =
      laneTermination.ExtentPositions();
    points.push_back(extents[0]);
    points.push_back(extents[1]);
  }
  // Create the center of the junction
  ignition::math::Vector3d &center = pose.Pos();
  center = gazebo::GetCenter(points);
  pose.Rot() = ignition::math::Quaterniond(0.0, 0.0, 0.0);
  // Add the Z coordinate to the points
  std::for_each(points.begin(), points.end(),
    [](ignition::math::Vector3d& v) {
      v.Z() = Creator::DEFAULT_HEIGHT;
    });
  // Polar sort on the points
  std::sort(points.begin(), points.end(), gazebo::PolarSort(center));
}

void Junction::PrintPoints() const {
  for (const auto &point : points) {
    std::cout << point.X() << "," << point.Y() << std::endl;
  }
}

ignition::math::Pose3d& Junction::Pose() {
  return pose;
}

std::vector<ignition::math::Vector3d>& Junction::Points() {
  return points;
}

bool Junction::ContainsAny(
  const std::vector<ignition::rndf::UniqueId> &ids) const {
  std::vector<ignition::rndf::UniqueId> junctionIds;
  for (const auto &laneTermination : laneTerminations) {
    junctionIds.push_back(laneTermination.Id());
  }
  for (const auto &id : ids) {
    if (std::find_if(junctionIds.begin(),
          junctionIds.end(),
          gazebo::LaneTermination::IdMatch(id)) !=
        junctionIds.end()) {
      return true;
    }
  }
  return false;
}

int Junction::GetJunctionIndexById(
  const std::vector<gazebo::Junction> &junctions,
  const std::vector<gazebo::LaneTermination> &lts) {
  std::vector<ignition::rndf::UniqueId> ids;
  std::for_each(lts.begin(), lts.end(),
    [&](const gazebo::LaneTermination &lt) {
      ids.push_back(lt.Id());
    });
  // Check if there is a match by ids
  for (uint i = 0; i < junctions.size(); i++) {
    if (junctions[i].ContainsAny(ids))
      return static_cast<int>(i);
  }
  return -1;
}

JunctionCreator::JunctionCreator() : Creator() {
}

JunctionCreator::~JunctionCreator() {
}

void JunctionCreator::Junction(const gazebo::Junction &_junction) {
  this->junction = _junction;
}

void JunctionCreator::Material(const std::string &_material) {
  this->material = _material;
}

void JunctionCreator::SetMeshCreator(gazebo::MeshCreator *_meshCreator) {
  this->meshCreator = _meshCreator;
}

bool JunctionCreator::Create() {
  junction.Compute();
  // Internal function to compute the polar sort of the three points.
  auto addVertexesToMesh = [](const ignition::math::Vector3d &a,
    const ignition::math::Vector3d &b,
    const ignition::math::Vector3d &c) mutable {
    std::vector<ignition::math::Vector3d> points = {a, b, c};
    gazebo::OrderPolar(points);
    return points;
  };
  // Iterate over all the points to get the triangles of the mesh.
  for (uint i = 0; i < (junction.Points().size() - 1); i++) {
    auto vertexes = addVertexesToMesh(junction.Pose().Pos(),
      junction.Points()[i], junction.Points()[i + 1]);
    meshCreator->AddVertexes(vertexes);
  }
  auto vertexes = addVertexesToMesh(junction.Pose().Pos(),
    junction.Points().front(), junction.Points().back());
  meshCreator->AddVertexes(vertexes);

  return true;
}

}
