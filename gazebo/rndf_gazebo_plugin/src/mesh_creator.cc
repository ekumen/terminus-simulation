/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#include "mesh_creator.hh"

namespace gazebo {

MeshCreator::MeshCreator() {
}

MeshCreator::~MeshCreator() {
}

void MeshCreator::Name(const std::string &_name) {
  this->name = _name;
}

void MeshCreator::AddVertexes(
  const std::vector<ignition::math::Vector3d> &_vertexes) {
  this->vertexes.insert(this->vertexes.end(), _vertexes.begin(),
    _vertexes.end());
}

gazebo::common::Mesh* MeshCreator::Create() const {
  gazebo::common::SubMesh* subMesh = new gazebo::common::SubMesh();
  subMesh->SetName(std::string("submesh_") + this->name);
  subMesh->SetPrimitiveType(gazebo::common::SubMesh::TRIANGLES);
  for (uint i = 0; i < this->vertexes.size(); i++) {
    subMesh->AddVertex(this->vertexes[i]);
    subMesh->AddNormal(ignition::math::Vector3d(0., 0., 1.));
    subMesh->AddIndex(i);
  }
  gazebo::common::Mesh* mesh = new gazebo::common::Mesh();
  mesh->SetName(std::string("mesh_") + this->name);
  mesh->AddSubMesh(subMesh);
  return mesh;
}

void MeshCreator::ClearVertexes() {
  this->vertexes.clear();
}

}
