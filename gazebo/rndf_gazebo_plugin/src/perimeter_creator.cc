/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#include "perimeter_creator.hh"

#include <algorithm>
#include "math_util.hh"


namespace gazebo {

PerimeterCreator::PerimeterCreator() : Creator() {
  nodePtr.reset(new ignition::transport::Node);
}

PerimeterCreator::~PerimeterCreator() {
  nodePtr.reset();
}

bool PerimeterCreator::Create() {
  if (verteces.empty())
    return false;

  auto addVertexesToMesh = [](const ignition::math::Vector3d &a,
    const ignition::math::Vector3d &b,
    const ignition::math::Vector3d &c) mutable {
    std::vector<ignition::math::Vector3d> points = {a, b, c};
    gazebo::OrderPolar(points);
    return points;
  };

  for (uint i = 0; i < (this->verteces.size() - 1); i++) {
    auto points = addVertexesToMesh(this->verteces.front(), this->verteces[i],
      this->verteces[i + 1]);
    meshCreator->AddVertexes(points);
  }
  auto points = addVertexesToMesh(this->verteces.front(), this->verteces[1],
    this->verteces.back());
  meshCreator->AddVertexes(points);

  return true;
}

void PerimeterCreator::Material(const std::string &_material) {
  this->material = _material;
}

void PerimeterCreator::SetMeshCreator(gazebo::MeshCreator *_meshCreator) {
  this->meshCreator = _meshCreator;
}

void PerimeterCreator::Verteces(
  const std::vector<ignition::math::Vector3d> &_verteces) {
  // keep verteces in triangle fan representation
  if (_verteces.size() < 3) {
    // not enough points to be a closed shape
    return;
  }
  this->verteces.clear();
  // push center
  this->verteces.push_back(GetCenter(_verteces));
  // push all verteces in reverse order
  this->verteces.insert(this->verteces.end(),
    _verteces.rbegin(), _verteces.rend());
  // add fixed z-coord to all verteces
  std::for_each(this->verteces.begin(), this->verteces.end(),
    [](ignition::math::Vector3d& v) {
      v.Z() = Creator::DEFAULT_HEIGHT;
    });
}

}
