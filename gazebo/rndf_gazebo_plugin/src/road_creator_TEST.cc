/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#include <gazebo/gazebo.hh>
#include <ignition/math.hh>

#include <vector>

#include "creator.hh"
#include "road_creator.hh"

#include "gtest/gtest.h"

#define DOUBLE_TOLERANCE  0.00001

TEST(ROAD_CREATOR_Test, NumberOfPoses)
{
  gazebo::RoadCreator roadCreator;
  std::vector<ignition::math::Vector3d> points, trianglePoints;
  std::vector<ignition::math::Pose3d> poses;
  points.push_back(ignition::math::Vector3d(0.0, 0.0, 0.0));
  points.push_back(ignition::math::Vector3d(1.0, 0.0, 0.0));

  roadCreator.FillPoses(points, poses);

  EXPECT_EQ(poses.size(), static_cast<uint>(2));
}

TEST(ROAD_CREATOR_Test, NumberOfPoses2)
{
  gazebo::RoadCreator roadCreator;
  std::vector<ignition::math::Vector3d> points, trianglePoints;
  std::vector<ignition::math::Pose3d> poses;
  points.push_back(ignition::math::Vector3d(0.0, 0.0, 0.0));
  points.push_back(ignition::math::Vector3d(1.0, 0.0, 0.0));
  points.push_back(ignition::math::Vector3d(2.0, 0.0, 0.0));
  roadCreator.FillPoses(points, poses);

  EXPECT_EQ(poses.size(), static_cast<uint>(3));
}

TEST(ROAD_CREATOR_Test, LastTwoRotsAreEqual)
{
  gazebo::RoadCreator roadCreator;
  std::vector<ignition::math::Vector3d> points, trianglePoints;
  std::vector<ignition::math::Pose3d> poses;
  points.push_back(ignition::math::Vector3d(0.0, 0.0, 0.0));
  points.push_back(ignition::math::Vector3d(1.0, 0.0, 0.0));
  points.push_back(ignition::math::Vector3d(2.0, 0.0, 0.0));
  roadCreator.FillPoses(points, poses);

  EXPECT_EQ(poses[1].Rot(), poses[2].Rot());
}

TEST(ROAD_CREATOR_Test, PositionsAreOkInPoses)
{
  gazebo::RoadCreator roadCreator;
  std::vector<ignition::math::Vector3d> points, trianglePoints;
  std::vector<ignition::math::Pose3d> poses;
  points.push_back(ignition::math::Vector3d(0.0, 0.0, 0.0));
  points.push_back(ignition::math::Vector3d(1.0, 0.0, 0.0));
  points.push_back(ignition::math::Vector3d(2.0, 0.0, 0.0));
  points.push_back(ignition::math::Vector3d(4.0, 0.0, 0.0));
  roadCreator.FillPoses(points, poses);

  for (uint i = 0; i < points.size(); i++) {
    EXPECT_EQ(points[i], poses[i].Pos());
  }
}


TEST(ROAD_CREATOR_Test, NumberOfPosesException)
{
  gazebo::RoadCreator roadCreator;
  std::vector<ignition::math::Vector3d> points, trianglePoints;
  std::vector<ignition::math::Pose3d> poses;

  EXPECT_THROW(roadCreator.FillPoses(points, poses),
    gazebo::common::Exception);

  points.push_back(ignition::math::Vector3d(0.0, 0.0, 0.0));
  EXPECT_THROW(roadCreator.FillPoses(points, poses),
    gazebo::common::Exception);
}

TEST(ROAD_CREATOR_Test, TriangPointsSize)
{
  gazebo::RoadCreator roadCreator;
  std::vector<ignition::math::Vector3d> points, trianglePoints;
  std::vector<ignition::math::Pose3d> poses;
  points.push_back(ignition::math::Vector3d(0.0, 0.0, 0.0));
  points.push_back(ignition::math::Vector3d(1.0, 0.0, 0.0));

  roadCreator.FillPoses(points, poses);
  roadCreator.FillTriangleStripPoints(
    1.0,
    poses,
    trianglePoints);
  EXPECT_EQ(trianglePoints.size(), static_cast<std::size_t>(6));
}

TEST(ROAD_CREATOR_Test, RoadSegmentPoints)
{
  gazebo::RoadCreator roadCreator;
  std::vector<ignition::math::Vector3d> points, trianglePoints,
    realTrianglePoints;
  std::vector<ignition::math::Pose3d> poses;
  points.push_back(ignition::math::Vector3d(0.0, 0.0, 0.0));

  points.push_back(ignition::math::Vector3d(1.0, 0.0, 0.0));

  realTrianglePoints.push_back(
    ignition::math::Vector3d(0.0, -0.5, 0.0));
  realTrianglePoints.push_back(
    ignition::math::Vector3d(0.0, 0.5, 0.0));
  realTrianglePoints.push_back(
    ignition::math::Vector3d(1.0, 0.5, 0.0));
  realTrianglePoints.push_back(
    ignition::math::Vector3d(1.0, -0.5, 0.0));
  gazebo::OrderPolar(realTrianglePoints);

  roadCreator.FillPoses(points, poses);
  roadCreator.FillTriangleStripPoints(
    1.0,
    poses,
    trianglePoints);

  EXPECT_NEAR(trianglePoints[0].
    Distance(realTrianglePoints[0]),
    0.0,
    DOUBLE_TOLERANCE);
  EXPECT_NEAR(trianglePoints[1].
    Distance(realTrianglePoints[1]),
    0.0,
    DOUBLE_TOLERANCE);
  EXPECT_NEAR(trianglePoints[2].
    Distance(realTrianglePoints[2]),
    0.0,
    DOUBLE_TOLERANCE);
  EXPECT_NEAR(trianglePoints[3].
    Distance(realTrianglePoints[0]),
    0.0,
    DOUBLE_TOLERANCE);
  EXPECT_NEAR(trianglePoints[4].
    Distance(realTrianglePoints[2]),
    0.0,
    DOUBLE_TOLERANCE);
  EXPECT_NEAR(trianglePoints[5].
    Distance(realTrianglePoints[3]),
    0.0,
    DOUBLE_TOLERANCE);
}

//////////////////////////////////////////////////
int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
