#!/bin/bash

source /usr/share/gazebo-8/setup.sh
export TERMINUS_SIMULATION_PATH=/home/gazebo/ws/terminus-simulation
export TERMINUS_SAMPLES_PATH=/home/gazebo/ws/samples/gazebo_8
export GAZEBO_RESOURCE_PATH=${TERMINUS_SIMULATION_PATH}/example:${GAZEBO_RESOURCE_PATH}
export GAZEBO_RESOURCE_PATH=${TERMINUS_SAMPLES_PATH}:${GAZEBO_RESOURCE_PATH}
export GAZEBO_PLUGIN_PATH=${GAZEBO_PLUGIN_PATH}:${TERMINUS_SIMULATION_PATH}/build/rndf_gazebo_plugin/src:${TERMINUS_SIMULATION_PATH}/build/road_driver_plugin/src
export GAZEBO_MODEL_PATH=${GAZEBO_MODEL_PATH}:${TERMINUS_SIMULATION_PATH}/models
