#!/bin/sh

# Exit if any of the commands fails
set -e

git stash -q --keep-index
sh ./tools/code_check.sh
git stash pop -q
